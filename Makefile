path = docker

clean:
	docker system prune -a -f

start:
	docker-compose up --build -d

build:
	docker-compose build

ps:
	docker-compose ps

down:
	docker-compose down

up:
	docker-compose up -d

reload: down up
