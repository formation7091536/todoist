import { combineReducers } from "@reduxjs/toolkit";
import todoProvider from "../../pages/dashboard/data/todoSlice";

const rootReducer = combineReducers({
  todoProvider,
});

export default rootReducer;
