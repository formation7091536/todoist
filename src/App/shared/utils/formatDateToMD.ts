const months = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December",
];
export default function (date: string): string {
  const dateObj = new Date(JSON.parse(date));
  return months[dateObj.getMonth()].slice(0, 3) + " " + dateObj.getDate();
}
