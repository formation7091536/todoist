import { createClient } from "@supabase/supabase-js";

const supabaseUrl = import.meta.env.VITE_APP_BASE_URL;
const supabaseKey = import.meta.env.VITE_APP_API_KEY;
export default createClient(supabaseUrl, supabaseKey);
