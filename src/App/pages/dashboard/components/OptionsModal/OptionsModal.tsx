import {
  EditIcon,
  TrashIcon,
  DuplicateIcon,
} from "../../../../shared/assets/Icons";
import Modal from "../Modal";
import "./OptionsModal.scss";

export default function OptionsModal({
  editTask,
  duplicateTask,
  deleteTaskById,
  visibility,
  closeMe,
}: Readonly<{
  editTask: () => void;
  duplicateTask: () => void;
  deleteTaskById: () => void;
  visibility: boolean;
  closeMe: () => void;
}>) {
  return (
    <Modal
      visibility={visibility}
      toggleFn={closeMe}
      position={{ top: "2.25rem", left: "50%" }}
    >
      <div className="modal-container">
        <button className="row" onClick={editTask}>
          <span className="icon">
            <EditIcon />
          </span>
          <span className="title">Edit</span>
        </button>
        <button className="row" onClick={duplicateTask}>
          <span className="icon">
            <DuplicateIcon />
          </span>
          <span className="title">Duplicate</span>
        </button>
        <button className="row delete-row" onClick={deleteTaskById}>
          <span className="icon">
            <TrashIcon />
          </span>
          <span className="title">Delete</span>
        </button>
      </div>
    </Modal>
  );
}
