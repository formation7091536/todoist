export interface HeaderProps {
  startDate: number;
  goPrevWeek: () => void;
  goToday: () => void;
  goNextWeek: () => void;
  setStartDate: (startDate: number) => void;
}
