import { differenceInDays, startOfWeek } from "date-fns";
import { useEffect, useState } from "react";
import { BellIcon, DownArrowIcon } from "../../../../shared/assets/Icons";
import { useAppDispatch, useAppSelector } from "../../../../shared/store";
import { getNotifications } from "../../data/todoThunk";
import Calendar from "../Calendar";
import Modal from "../Modal";
import NotificationStack from "../NotificationStack/";
import { HeaderProps } from "./header.types";

export default function Header({
  startDate,
  goPrevWeek,
  goToday,
  goNextWeek,
  setStartDate,
}: Readonly<HeaderProps>) {
  const [showCalendar, setShowCalendar] = useState(false);
  const [selectedDate, setSelectedDate] = useState(
    new Date(new Date().getTime() + startDate * 24 * 60 * 60 * 1000),
  );
  const month = selectedDate.toLocaleString("default", { month: "long" });
  const year = selectedDate.getFullYear();

  const [showNotification, setShowNotification] = useState(false);
  const numberOfNotifications = useAppSelector(
    (store) =>
      store.todoProvider.notifications.filter((notif) => !notif.is_read).length,
  );
  const totodo = useAppSelector((store) => store.todoProvider);

  const dispatch = useAppDispatch();

  useEffect(() => {
    setTimeout(() => dispatch(getNotifications()), 1000);
  }, [dispatch, totodo.columns, totodo.tasks]);

  return (
    <header className="header">
      <h3 className="primary-heading">Upcomming</h3>
      <div className="sub-header">
        <button
          className="date-picker-btn"
          onClick={(e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
            e.stopPropagation();
            setShowCalendar(true);
          }}
        >
          <span className="month">{month}</span>
          <span>{year}</span>
          <DownArrowIcon />
        </button>

        {showCalendar && (
          <Modal
            visibility={showCalendar}
            toggleFn={() => setShowCalendar(false)}
          >
            <Calendar
              value={selectedDate}
              onChange={(date) => {
                setSelectedDate(date);
                const weekStartAt = startOfWeek(date);
                const upcommingStartAt = new Date();
                const difference = differenceInDays(
                  weekStartAt,
                  upcommingStartAt,
                );
                setStartDate(difference > 0 ? difference + 1 : 0);
              }}
              closeCalendar={() => setShowCalendar(false)}
            ></Calendar>
          </Modal>
        )}

        <div className="pagination-actions">
          <button
            className={`btn btn-lt ${startDate === 0 ? "disabled" : ""}`}
            onClick={goPrevWeek}
            disabled={startDate === 0}
          >
            &larr;
          </button>
          <button className="btn today" onClick={goToday}>
            Today
          </button>
          <button className="btn btn-rt" onClick={goNextWeek}>
            &rarr;
          </button>
        </div>

        <button
          className="notification"
          onClick={(e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
            e.stopPropagation();
            setShowNotification(true);
          }}
        >
          <BellIcon />
          {numberOfNotifications > 0 && (
            <span className="count">{numberOfNotifications}</span>
          )}

          {showNotification && (
            <NotificationStack
              visibility={showNotification}
              toggleFn={() => {
                setShowNotification((prev) => !prev);
              }}
            />
          )}
        </button>
      </div>
    </header>
  );
}
