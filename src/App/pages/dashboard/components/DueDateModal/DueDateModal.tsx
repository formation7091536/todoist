import dueDays from "../../../../shared/data/dueDays";
import weekDays from "../../../../shared/data/weekDays";
import formatDateToMD from "../../../../shared/utils/formatDateToMD";
import Modal from "../Modal";
import "./DueDateModal.scss";

export default function DueDateModal({
  date,
  visibility,
  toggleFn,
  setDueDate,
}: Readonly<{
  date: string;
  visibility: boolean;
  toggleFn: () => void;
  setDueDate: (date: Date) => void;
}>) {
  return (
    <Modal
      visibility={visibility}
      toggleFn={toggleFn}
      position={{ top: "-450%" }}
    >
      <div className="modal">
        <header className="day-indicator">{formatDateToMD(date)}</header>
        <div className="due-date-indicators-container">
          {dueDays.map((dayObj) => {
            return (
              <button
                key={dayObj.day}
                className="due-date-indicator"
                onClick={() => {
                  setDueDate(dayObj.dueDate);
                  toggleFn();
                }}
              >
                <div className="left-side">
                  <span className="icon" style={{ color: dayObj.color }}>
                    {dayObj.icon(new Date().getDate())}
                  </span>
                  <span className="label">{dayObj.day}</span>
                </div>
                <span className="day-slug">
                  {dayObj.day === "Next week"
                    ? dayObj.dueDate.toDateString().slice(0, 10)
                    : weekDays[dayObj.dueDate.getDay()].slice(0, 3)}
                </span>
              </button>
            );
          })}
        </div>
      </div>
    </Modal>
  );
}
