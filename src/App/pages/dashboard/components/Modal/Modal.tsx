import { useCallback, useEffect, useRef, useState } from "react";
import "./Modal.scss";

export default function Modal({
  visibility,
  toggleFn,
  children,
  position,
}: Readonly<{
  visibility: boolean;
  toggleFn: () => void;
  children: React.ReactNode;
  position?: { top?: string; left?: string; right?: string; bottom?: string };
}>) {
  const [isVisible, setIsVisible] = useState(visibility);

  const modalRef = useRef<HTMLDivElement | null>(null);
  const closeModal = useCallback(() => {
    setIsVisible(false);
    toggleFn();
  }, [toggleFn]);

  const handleClickOutside = useCallback(
    (event: Event) => {
      event.stopPropagation();
      if (
        modalRef.current &&
        !(modalRef.current as unknown as HTMLElement).contains(
          event.target as Node,
        )
      ) {
        closeModal();
      }
    },
    [closeModal],
  );

  useEffect(() => {
    if (isVisible) {
      document.addEventListener("mousedown", handleClickOutside);
    }

    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, [isVisible, handleClickOutside]);

  useEffect(() => {
    const handleKeyDown = (event: KeyboardEvent) => {
      if (event.key === "Escape") {
        closeModal();
      }
    };

    document.addEventListener("keydown", handleKeyDown);

    return () => {
      document.removeEventListener("keydown", handleKeyDown);
    };
  }, [closeModal]);

  return (
    <>
      <div className="overlay"></div>
      <div
        className="modal-backdrop"
        style={{ display: isVisible ? "block" : "none", ...position }}
        ref={modalRef}
        aria-modal="true"
        aria-hidden={!isVisible}
      >
        {children}
      </div>
    </>
  );
}
