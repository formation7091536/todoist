import TaskItem from "./TaskItem";
import { TasksStackProps } from "./TasksStack.types";

export default function TasksStack({
  tasks,
  column,
}: Readonly<TasksStackProps>) {
  return (
    <div className="tasks">
      {tasks.map((task) => (
        <TaskItem key={task.id} task={task} column={column} />
      ))}
    </div>
  );
}
