import { useState } from "react";
import { toast } from "react-toastify";
import { HorizontalDotsIcon } from "../../../../shared/assets/Icons";
import { useAppDispatch } from "../../../../shared/store";
import { TaskPayload } from "../../data/todo.types";
import { createTask, deleteTask, markTaskAsDone } from "../../data/todoThunk";
import OptionsModal from "../OptionsModal";
import TaskInput from "../TaskInput";
import { TaskItemProps } from "./TasksStack.types";

const colors = [
  { color: "#D1453B", background: "#FAEBEA" },
  { color: "#EB8909", background: "#FCF2E5" },
  { color: "#4987E5", background: "#E7EFFB" },
  { color: "#b1b1b1", background: "#ffffff" },
];

export default function TaskItem({ task, column }: Readonly<TaskItemProps>) {
  const [editModalIsOpen, setEditModalIsOpen] = useState(false);
  const [isEditMode, setIsEditMode] = useState(false);
  const [isDragging, setIsDragging] = useState(false);

  const dispatch = useAppDispatch();

  function markAsDone(id: string) {
    dispatch(markTaskAsDone(id));
  }

  function openOptionsModal() {
    setEditModalIsOpen(true);
  }

  function closeOptionsModal() {
    setEditModalIsOpen(false);
  }

  function openEditMode() {
    setIsEditMode(true);
  }

  function closeEditMode() {
    setIsEditMode(false);
    closeOptionsModal();
  }

  async function duplicateTask() {
    const duplicatedTaskPaylod: TaskPayload = {
      ...task,
      due_date: new Date(task.due_date),
    };
    delete duplicatedTaskPaylod.id;
    delete duplicatedTaskPaylod.created_at;
    console.log(duplicatedTaskPaylod);
    const res = await dispatch(createTask(duplicatedTaskPaylod));

    console.log(res);
    if (res.type.endsWith("fulfilled")) {
      toast.success("Task duplicated successfully!", {
        position: "bottom-center",
        className: "toast",
      });
    } else if (res.type.endsWith("rejected")) {
      toast.error("Could not duplicate task!", {
        position: "bottom-center",
        className: "toast",
      });
    }

    closeOptionsModal();
  }

  function deleteTaskBy(id: string) {
    return () => {
      dispatch(deleteTask(id));
      closeOptionsModal();
    };
  }

  function handleDrag(ev: React.DragEvent<HTMLDivElement>): void {
    setIsDragging(true);
    const id = (ev.target as HTMLDivElement).id;
    ev.dataTransfer.setData("text/plain", id);
  }

  const { color, background: backgroundColor } = task.priority
    ? colors[task.priority - 1]
    : colors[3];

  const border =
    task.priority && task.priority !== 4
      ? `2px solid ${color}`
      : `1px solid ${color}`;

  return !isEditMode ? (
    <div
      className="task"
      key={task.id}
      draggable={true}
      onDragStart={handleDrag}
      onDragLeave={() => setIsDragging(false)}
      id={task.id}
      style={{
        opacity: isDragging ? 0.5 : 1,
        cursor: isDragging ? "grab" : "default",
      }}
    >
      <button
        className="btn-mark-task-done"
        onClick={() => markAsDone(task.id)}
        style={
          task.priority && task.priority !== 4
            ? {
                color,
                border,
                backgroundColor,
              }
            : {}
        }
      >
        <div
          className="check"
          style={{ borderBottom: border, borderRight: border }}
        ></div>
      </button>
      <div>
        <p className="task-name">{task.title}</p>
        <p className="task-description">{task.description}</p>
      </div>
      <button className="btn-edit-task" onClick={openOptionsModal}>
        <HorizontalDotsIcon />
      </button>
      {editModalIsOpen && (
        <OptionsModal
          editTask={openEditMode}
          duplicateTask={duplicateTask}
          deleteTaskById={deleteTaskBy(task.id)}
          visibility={editModalIsOpen}
          closeMe={closeOptionsModal}
        />
      )}
    </div>
  ) : (
    <TaskInput
      type="edit"
      closeTaskInputCard={closeEditMode}
      taskId={task.id}
      column={column}
    />
  );
}
