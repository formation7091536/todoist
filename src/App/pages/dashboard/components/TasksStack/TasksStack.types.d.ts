import { Column, Task } from "../../data/todo.types";

export interface TasksStackProps {
  tasks: Task[];
  column: Column;
}

export interface TaskItemProps {
  task: Task;
  column: Column;
}
