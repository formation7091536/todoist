import { add, format, isBefore, startOfMonth, sub } from "date-fns";
import Cell from "./Cell";
import MonthBox from "./MonthBox";
import "./index.scss";

type Props = {
  value?: Date;
  onChange: (date: Date) => void;
  closeCalendar: () => void;
};

const Calendar: React.FC<Props> = ({
  value = new Date(),
  onChange,
  closeCalendar,
}) => {
  const prevThreeMonths = () =>
    onChange(
      isBefore(sub(value, { months: 3 }), new Date())
        ? new Date()
        : sub(value, { months: 3 }),
    );
  const nextThreeMonths = () =>
    onChange(startOfMonth(add(value, { months: 3 })));
  const prevYear = () =>
    onChange(
      isBefore(sub(value, { years: 1 }), new Date())
        ? new Date()
        : sub(value, { years: 1 }),
    );
  const nextYear = () => onChange(add(value, { years: 1 }));

  const handleClickDate = (date: Date) => {
    onChange(date);
    closeCalendar();
  };

  return (
    <div className="calendar-box">
      <header className="calendar-box--header">
        <Cell className="month" key={"d"}>
          {format(value, "LLLL yyyy")}
        </Cell>
        <div className="calendar-box--actions">
          <Cell onClick={prevYear} key={"<<"}>
            {"<<"}
          </Cell>
          <Cell onClick={prevThreeMonths} key={"<"}>
            {"<"}
          </Cell>
          <Cell onClick={nextThreeMonths} key={">"}>
            {">"}
          </Cell>
          <Cell onClick={nextYear} key={">>"}>
            {">>"}
          </Cell>
        </div>
      </header>

      <MonthBox value={value} selectDate={handleClickDate}></MonthBox>
      <MonthBox
        value={value}
        selectDate={handleClickDate}
        addValue={1}
      ></MonthBox>
      <MonthBox
        value={value}
        selectDate={handleClickDate}
        addValue={2}
      ></MonthBox>
    </div>
  );
};

export default Calendar;
