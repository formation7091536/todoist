import {
  add,
  differenceInDays,
  endOfMonth,
  // isBefore,
  isSameMonth,
  setDate,
  startOfMonth,
} from "date-fns";
import months from "../../../../shared/data/months";
import weekDays from "../../../../shared/data/weekDays";
import Cell from "./Cell";

export default function MonthBox({
  value,
  selectDate,
  addValue,
}: Readonly<{
  value: Date;
  selectDate: (date: Date) => void;
  addValue?: number;
}>) {
  const oldValue = value;
  value = add(value, { months: addValue });
  const startDate =
    addValue === undefined && isSameMonth(new Date(), value)
      ? new Date()
      : startOfMonth(value);
  const endDate = endOfMonth(value);
  const numDays = differenceInDays(endDate, startDate) + 1;

  const prefixDays = startDate.getDay();
  const suffixDays = 6 - endDate.getDay();

  function handleClickDate(index: number) {
    const date = setDate(value, index);
    selectDate(date);
  }

  return (
    <>
      {addValue !== undefined && (
        <span className="month">{months[value.getMonth()]}</span>
      )}
      <div className="month-box">
        {weekDays.map((day, index) => (
          <Cell className="" key={index + new Date().getTime()}>
            {day.at(0)}
          </Cell>
        ))}

        {Array.from({ length: prefixDays }).map((_, index) => {
          const date = startDate.getDate() - (prefixDays - index);
          return (
            <Cell className={""} key={index + new Date().getTime()}>
              {date > 0 ? date : ""}
            </Cell>
          );
        })}

        {Array.from({ length: numDays }).map((_, index) => {
          const date = startDate.getDate() + index;
          const isCurrentDate = date === value.getDate();

          return (
            <Cell
              className={`cursor-pointer ${oldValue.getMonth() === value.getMonth() && oldValue.getDate() === date ? "active" : ""}`}
              key={date}
              isActive={isCurrentDate}
              onClick={() => handleClickDate(date)}
            >
              {date}
            </Cell>
          );
        })}

        {Array.from({ length: suffixDays }).map((_, index) => (
          <Cell className="" key={index + new Date().getTime()} />
        ))}
      </div>
    </>
  );
}
