import clsx from "clsx";
import "./index.scss";

interface Props extends React.PropsWithChildren {
  className?: string;
  isActive?: boolean;
  onClick?: () => void;
}

const Cell: React.FC<Props> = ({
  onClick,
  children,
  className,
  isActive = false,
}) => {
  return (
    <button
      onClick={!isActive ? onClick : undefined}
      className={clsx(
        "cell h-10 border-b border-r flex items-center justify-center select-none transition-colors",
        {
          "want-to-select cursor-pointer hover:bg-gray-100 active:bg-gray-200":
            !isActive && onClick,
          "selected font-bold text-white bg-blue-600": isActive,
        },
        className,
      )}
    >
      <div className="cell-day">{children}</div>
    </button>
  );
};

export default Cell;
