import { useAppSelector } from "../../../../shared/store";
import { Notification } from "../../data/todo.types";
import Modal from "../Modal";
import NotificationItem from "./NotificationItem";
import "./NotificationStack.scss";

export default function NotificationStack({
  visibility,
  toggleFn,
}: Readonly<{ visibility: boolean; toggleFn: () => void }>) {
  const notifications = useAppSelector(
    (store) => store.todoProvider.notifications,
  );
  return (
    <Modal
      visibility={visibility}
      toggleFn={toggleFn}
      position={{ top: "2rem", left: "-10rem" }}
    >
      <div className="stack">
        {notifications.length === 0 && <span>You don't miss anything!</span>}
        {notifications.map((notification: Notification) => (
          <NotificationItem
            key={notification.id}
            notification={notification}
            toggleFn={toggleFn}
          />
        ))}
      </div>
    </Modal>
  );
}
