import { useAppDispatch } from "../../../../shared/store";
import { Notification } from "../../data/todo.types";
import { markNotificationAsRead } from "../../data/todoThunk";

export default function NotificationItem({
  notification,
  toggleFn,
}: Readonly<{ notification: Notification; toggleFn: () => void }>) {
  const dispatch = useAppDispatch();

  function handleMarkAsRead(e: React.MouseEvent<HTMLDivElement, MouseEvent>) {
    e.stopPropagation();
    dispatch(markNotificationAsRead(notification.id));
    toggleFn();
  }

  function goToTask(e: React.MouseEvent<HTMLDivElement, MouseEvent>) {
    e.stopPropagation();
    const taskItem = document.getElementById(notification.task.id);
    if (taskItem) taskItem.setAttribute("style", "background-color: #f5f5f5");
    setTimeout(() => {
      taskItem?.setAttribute("style", "background-color: #fff");
    }, 1000);
    toggleFn();
  }

  return (
    <div
      className={`stack-item ${notification.is_read ? "is-read" : ""}`}
      onClick={goToTask}
    >
      <span className="label">{notification.task.title} &rarr;</span>
      <div className="btn-mark-task-done" onClick={handleMarkAsRead}>
        <div className="check"></div>
      </div>
    </div>
  );
}
