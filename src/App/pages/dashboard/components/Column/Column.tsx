import { isSameDay } from "date-fns";
import { PlusSignIcon } from "../../../../shared/assets/Icons";
import store, {
  useAppDispatch,
  useAppSelector,
} from "../../../../shared/store";
import formatDateToMD from "../../../../shared/utils/formatDateToMD";
import { Task } from "../../data/todo.types";
import { setModalIsClose, setModalIsOpen } from "../../data/todoSlice";
import { updateTask } from "../../data/todoThunk";
import TaskInput from "../TaskInput";
import TasksStack from "../TasksStack";
import { ColumnProps } from "./Column.types";

export default function Column({ column }: Readonly<ColumnProps>) {
  const dispatch = useAppDispatch();
  const tasks = useAppSelector((state) =>
    state.todoProvider.tasks.filter(
      (task: Task) =>
        task.due_date === JSON.parse(column.date).split("T")[0] &&
        !task.completed,
    ),
  );

  function openAddTask() {
    dispatch(setModalIsOpen({ id: column.id }));
  }

  function closeAddTask() {
    dispatch(setModalIsClose({ id: column.id }));
  }

  function handleDragOver(e: React.DragEvent<HTMLDivElement>) {
    e.preventDefault();
  }

  function handleDrop(e: React.DragEvent<HTMLDivElement>) {
    const taskId = e.dataTransfer.getData("text/plain");
    const columnDate = new Date(JSON.parse(column.date));
    const task = store
      .getState()
      .todoProvider.tasks.find((task) => task.id === taskId);
    if (!task || isSameDay(new Date(task.due_date), columnDate)) return;
    dispatch(updateTask({ id: taskId, due_date: columnDate }));
  }

  return (
    <div
      className="column"
      id={column.id}
      onDragOver={handleDragOver}
      onDrop={handleDrop}
    >
      <h2 className="secondary-heading">
        {formatDateToMD(column.date)}
        {" - "}
        {column.title}
      </h2>
      {tasks.length > 0 && <TasksStack tasks={tasks} column={column} />}
      {!column.modalIsOpen ? (
        <button className="btn-add-task" onClick={openAddTask}>
          <span className="plus">
            <PlusSignIcon />
          </span>{" "}
          Add task
        </button>
      ) : (
        <TaskInput
          type="add"
          closeTaskInputCard={closeAddTask}
          column={column}
        />
      )}
    </div>
  );
}
