import { Column } from "../../data/todo.types";
export interface ColumnProps {
  key: string;
  column: Column;
}
