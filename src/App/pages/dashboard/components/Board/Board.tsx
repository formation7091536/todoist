import { useAppSelector } from "../../../../shared/store";
import Column from "../Column";

export default function Board() {
  const columns = useAppSelector((state) => state.todoProvider.columns);

  return (
    <div className="board">
      {columns.map((column) => (
        <Column key={column.id} column={column} />
      ))}
    </div>
  );
}
