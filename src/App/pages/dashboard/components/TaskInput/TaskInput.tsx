import { ChangeEvent, useEffect, useState } from "react";
import {
  ArrowIcon,
  CrossIcon,
  DueDayIcon,
  ReminderIcon,
} from "../../../../shared/assets/Icons";
import dueDays from "../../../../shared/data/dueDays";
import priorityFlags from "../../../../shared/data/priorityFlags";
import weekDays from "../../../../shared/data/weekDays";
import store, { useAppDispatch } from "../../../../shared/store";
import formatDateToMD from "../../../../shared/utils/formatDateToMD";
import { setModalIsClose } from "../../data/todoSlice";
import { createTask, updateTask } from "../../data/todoThunk";
import DueDateModal from "../DueDateModal";
import PriorityModal from "../PriorityModal";
import { TaskInputProps } from "./TaskInput.types";

export default function TaskInput({
  type,
  closeTaskInputCard,
  taskId,
  column,
}: Readonly<TaskInputProps>) {
  const task = store
    .getState()
    .todoProvider.tasks.filter((task) => task.id === taskId)[0];

  const [taskName, setTaskName] = useState(task?.title ?? "");
  const [taskDescription, setTaskDescription] = useState(
    task?.description ?? "",
  );
  const [dueDate, setDueDate] = useState(new Date(JSON.parse(column.date)));
  const [priority, setPriority] = useState<1 | 2 | 3 | 4>(task?.priority ?? 4);
  const [hasReminder, setHasReminder] = useState(task?.has_reminder ?? false);

  const [priorityModalIsOpen, setPriorityModalIsOpen] = useState(false);
  const [dueDateModalIsOpen, setDueDateModalIsOpen] = useState(false);

  const dispatch = useAppDispatch();

  useEffect(() => {
    const reminder = document.querySelector(".reminder");
    reminder?.setAttribute(
      "data-hint",
      hasReminder
        ? "Cancel reminder"
        : "Set a reminder one day before due date",
    );
  }, [hasReminder]);

  let taskRows = Math.ceil(taskName.length / 26);
  if (taskRows > 10) {
    taskRows = 10;
  }

  let taskDescriptionRows = Math.ceil(taskDescription.length / 26);
  if (taskDescriptionRows > 9) {
    taskDescriptionRows = 9;
  }

  function resetTask() {
    setTaskName("");
    setTaskDescription("");
    setDueDate(new Date(JSON.parse(column.date)));
    setPriority(4);
  }

  function handleTaskNameChange(e: ChangeEvent) {
    setTaskName((e.target as HTMLInputElement).value);
  }

  function handleTaskDescriptionChange(e: ChangeEvent) {
    setTaskDescription((e.target as HTMLInputElement).value);
  }

  function closeTaskCard() {
    resetTask();
    closeTaskInputCard();
  }

  function handleSubmission() {
    type === "add"
      ? dispatch(
          createTask({
            title: taskName,
            description: taskDescription,
            completed: false,
            due_date: dueDate,
            priority,
            has_reminder: hasReminder,
          }),
        )
      : dispatch(
          updateTask({
            id: taskId,
            title: taskName,
            description: taskDescription,
            due_date: dueDate,
            priority,
            has_reminder: hasReminder,
          }),
        );

    resetTask();
    dispatch(setModalIsClose({ id: column.id }));
    closeTaskInputCard();
  }

  function handleOpenDueDateModal() {
    setDueDateModalIsOpen(true);
  }

  function handleCloseDueDateModal() {
    setDueDateModalIsOpen(false);
  }

  function handleOpenPriorityModal() {
    setPriorityModalIsOpen(true);
  }

  function handleClosePriorityModal() {
    setPriorityModalIsOpen(false);
  }

  const dueDayObj = dueDays.filter(
    (day) => day.dueDate.getDate() === dueDate.getDate(),
  )[0];

  return (
    <div className="card-add-task">
      <div className="tags-container">
        {dueDayObj && dueDayObj.day !== column.day && (
          <span className="tag">
            {formatDateToMD(JSON.stringify(dueDayObj.dueDate))}
          </span>
        )}

        {priority !== 4 && <span className="tag">p{priority}</span>}

        {hasReminder && (
          <span className="tag">
            <ReminderIcon />
          </span>
        )}
      </div>

      <textarea
        id="task-name"
        placeholder="Task name"
        className={`title max-height-${taskRows}`}
        value={taskName}
        onChange={handleTaskNameChange}
      />
      <textarea
        id="task-description"
        placeholder="Description"
        className={`description max-height-${taskDescriptionRows}`}
        value={taskDescription}
        onChange={handleTaskDescriptionChange}
      />
      <div className="add-task--options">
        {dueDayObj &&
        (dueDayObj.day === "Today" || dueDayObj.day === "Tomorrow") ? (
          <button
            className="due-date"
            onClick={handleOpenDueDateModal}
            style={{ color: dueDayObj.color }}
          >
            <DueDayIcon />
            <span className="label">{dueDayObj.day}</span>
          </button>
        ) : (
          <button className="due-date" onClick={handleOpenDueDateModal}>
            <DueDayIcon />
            <span className="label">
              {dueDayObj ? weekDays[dueDayObj.dueDate.getDay()] : column.day}
            </span>
          </button>
        )}

        {dueDateModalIsOpen && (
          <DueDateModal
            date={dueDayObj ? JSON.stringify(dueDayObj.dueDate) : column.date}
            visibility={dueDateModalIsOpen}
            toggleFn={handleCloseDueDateModal}
            setDueDate={setDueDate}
          />
        )}

        <button className="priority-box" onClick={handleOpenPriorityModal}>
          {priorityFlags[priority - 1](16)}
          {`P${priority}`}
        </button>

        {priorityModalIsOpen && (
          <PriorityModal
            visibility={priorityModalIsOpen}
            toggleFn={handleClosePriorityModal}
            setPriority={setPriority}
          />
        )}

        <button
          className={`reminder ${hasReminder ? "active" : ""}`}
          onClick={() => setHasReminder((prev) => !prev)}
        >
          <ReminderIcon />
        </button>
      </div>

      <div className="card-add-task--actions">
        <button
          className="btn-action btn-cancel-add-task"
          onClick={closeTaskCard}
        >
          <CrossIcon />
        </button>
        <button
          className={`btn-action btn-confirm-add-task disabled`}
          disabled={!taskName}
          onClick={handleSubmission}
        >
          <ArrowIcon />
        </button>
      </div>
    </div>
  );
}
