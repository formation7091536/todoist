import { Column } from "../../data/todo.types";

export interface TaskInputProps {
  type: "add" | "edit";
  closeTaskInputCard: () => void;
  taskId?: string;
  column: Column;
}
