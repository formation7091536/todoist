import priorityFlags from "../../../../shared/data/priorityFlags";
import Modal from "../Modal";
import "./PriorityModal.scss";

export default function PriorityModal({
  visibility,
  toggleFn,
  setPriority,
}: Readonly<{
  visibility: boolean;
  toggleFn: () => void;
  setPriority: (priority: 1 | 2 | 3 | 4) => void;
}>) {
  return (
    <Modal
      visibility={visibility}
      toggleFn={toggleFn}
      position={{ top: "-400%", left: "25%" }}
    >
      <div className="priority-modal">
        {priorityFlags.map((flag, index) => (
          <button
            key={index}
            className="priority-flag"
            onClick={() => {
              setPriority((index + 1) as 1 | 2 | 3 | 4);
              toggleFn();
            }}
          >
            {flag(24)} Priority {index + 1}
          </button>
        ))}
      </div>
    </Modal>
  );
}
