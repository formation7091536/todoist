import { useEffect, useState } from "react";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { v4 as uuid4 } from "uuid";
import Header from "./components/Header";

import { useAppDispatch } from "../../shared/store";
import Board from "./components/Board";
import { initColumns } from "./data/todoSlice";
import { getAllTasks } from "./data/todoThunk";
import "./index.scss";

export default function Dashboard() {
  const [startDate, setStartDate] = useState(0);
  const dispatch = useAppDispatch();

  const days = [
    { title: "Sunday", day: "Sunday", label: "Sun" },
    { title: "Monday", day: "Monday", label: "Mon" },
    { title: "Tuesday", day: "Tuesday", label: "Tue" },
    { title: "Wednesday", day: "Wednesday", label: "Wed" },
    { title: "Thursday", day: "Thursday", label: "Thu" },
    { title: "Friday", day: "Friday", label: "Fri" },
    { title: "Saturday", day: "Saturday", label: "Sat" },
  ];

  const today = new Date();
  const startDayOfWeek = new Date(
    today.getTime() + startDate * 24 * 60 * 60 * 1000,
  );
  const day_index = startDayOfWeek.getDay();

  const columns = days.slice(day_index);

  if (startDate === 0) {
    columns[0].title = "Today";
    columns[1].title = "Tomorrow";
  }

  const stateColumns = columns.map((column, index) => ({
    id: uuid4(),
    date: JSON.stringify(
      new Date(startDayOfWeek.getTime() + index * 24 * 60 * 60 * 1000),
    ),
    modalIsOpen: false,
    title: column.title,
    day: column.day,
    label: column.label,
  }));

  useEffect(() => {
    dispatch(initColumns(stateColumns));
    dispatch(getAllTasks());
  }, [stateColumns, dispatch]);

  function goToday() {
    setStartDate(0);
  }

  function goNextWeek() {
    setStartDate((startDate) => startDate + 7 - day_index);
  }

  function goPrevWeek() {
    setStartDate((startDate) =>
      startDate - 7 + day_index > 0 ? startDate - 7 + day_index : 0,
    );
  }

  return (
    <div className="dashboard">
      <Header
        startDate={startDate}
        goToday={goToday}
        goNextWeek={goNextWeek}
        goPrevWeek={goPrevWeek}
        setStartDate={setStartDate}
      />

      <Board />

      <ToastContainer />
    </div>
  );
}
