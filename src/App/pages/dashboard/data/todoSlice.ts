import { PayloadAction, createSlice } from "@reduxjs/toolkit";
import { Column, Notification, Task, TodoState } from "./todo.types";
import {
  createTask,
  deleteTask,
  getAllTasks,
  getNotifications,
  markNotificationAsRead,
  markTaskAsDone,
  updateTask,
} from "./todoThunk";

const initialState = {
  status: "idle",
  tasks: [],
  columns: [],
  notifications: [],
  error: null,
  haveError: false,
} satisfies TodoState as TodoState;

const todoSlice = createSlice({
  name: "todoProvider",
  initialState,
  reducers: {
    initColumns(state, action: PayloadAction<Column[]>) {
      state.columns = action.payload;
    },
    addColumn(state, action: PayloadAction<Column>) {
      state.columns.push(action.payload);
    },
    setModalIsOpen(state, action: PayloadAction<{ id: string }>) {
      state.columns = state.columns.map((column) =>
        column.id === action.payload.id
          ? { ...column, modalIsOpen: true }
          : { ...column, modalIsOpen: false },
      );
    },
    setModalIsClose(state, action: PayloadAction<{ id: string }>) {
      state.columns = state.columns.map((column) =>
        column.id === action.payload.id
          ? { ...column, modalIsOpen: false }
          : column,
      );
    },
    addNotification(state, action: PayloadAction<Notification>) {
      state.notifications.push(action.payload);
    },
    restore(state) {
      state.columns = [];
      state.tasks = [];
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(createTask.pending, (state) => {
        state.status = "loading";
        state.error = null;
        state.haveError = false;
      })
      .addCase(createTask.fulfilled, (state, action: PayloadAction<Task>) => {
        state.tasks.push(action.payload);
        state.status = "succeeded";
      })
      .addCase(createTask.rejected, (state, action) => {
        state.status = "failed";
        state.error = action.error.message ?? "An error occurred";
        state.haveError = true;
      })
      .addCase(markTaskAsDone.pending, (state) => {
        state.status = "loading";
        state.error = null;
      })
      .addCase(
        markTaskAsDone.fulfilled,
        (state, action: PayloadAction<Task>) => {
          state.tasks = state.tasks.map((task) =>
            task.id === action.payload.id ? { ...task, completed: true } : task,
          );
          state.status = "succeeded";
        },
      )
      .addCase(markTaskAsDone.rejected, (state, action) => {
        state.status = "failed";
        state.error = action.error.message ?? "An error occurred";
        state.haveError = true;
      })
      .addCase(getAllTasks.pending, (state) => {
        state.error = null;
        state.status = "loading";
      })
      .addCase(
        getAllTasks.fulfilled,
        (state, action: PayloadAction<Task[]>) => {
          state.tasks = action.payload;
          state.status = "succeeded";
        },
      )
      .addCase(getAllTasks.rejected, (state, action) => {
        state.status = "failed";
        state.error = action.error.message ?? "An error occurred";
        state.haveError = true;
      })
      .addCase(deleteTask.pending, (state) => {
        state.error = null;
        state.status = "loading";
      })
      .addCase(
        deleteTask.fulfilled,
        (state, action: PayloadAction<{ id: string }>) => {
          state.tasks = state.tasks.filter(
            (task) => task.id !== action.payload.id,
          );
          state.status = "succeeded";
        },
      )
      .addCase(deleteTask.rejected, (state, action) => {
        state.status = "failed";
        state.error = action.error.message ?? "An error occurred";
        state.haveError = true;
      })
      .addCase(updateTask.pending, (state) => {
        state.error = null;
        state.status = "loading";
      })
      .addCase(updateTask.fulfilled, (state, action: PayloadAction<Task>) => {
        state.tasks = state.tasks.map((task) =>
          task.id === action.payload.id ? action.payload : task,
        );
        state.status = "succeeded";
      })
      .addCase(updateTask.rejected, (state, action) => {
        state.status = "failed";
        state.error = action.error.message ?? "An error occurred";
        state.haveError = true;
      })
      .addCase(getNotifications.pending, (state) => {
        state.error = null;
        state.status = "loading";
      })
      .addCase(
        getNotifications.fulfilled,
        (state, action: PayloadAction<Notification[]>) => {
          state.notifications = action.payload;
          state.status = "succeeded";
        },
      )
      .addCase(getNotifications.rejected, (state, action) => {
        state.status = "failed";
        state.error = action.error.message ?? "An error occurred";
        state.haveError = true;
      })
      .addCase(markNotificationAsRead.pending, (state) => {
        state.error = null;
        state.status = "loading";
      })
      .addCase(
        markNotificationAsRead.fulfilled,
        (state, action: PayloadAction<{ id: string }>) => {
          state.notifications = state.notifications.map((notification) =>
            notification.id === action.payload.id
              ? { ...notification, is_read: true }
              : notification,
          );
          state.status = "succeeded";
        },
      )
      .addCase(markNotificationAsRead.rejected, (state, action) => {
        state.status = "failed";
        state.error = action.error.message ?? "An error occurred";
        state.haveError = true;
      });
  },
});

export const {
  initColumns,
  addColumn,
  setModalIsOpen,
  setModalIsClose,
  addNotification,
  restore,
} = todoSlice.actions;
export default todoSlice.reducer;
