export interface TaskPayload {
  id?: string;
  title?: string;
  description?: string;
  completed?: boolean;
  due_date?: Date;
  priority?: 1 | 2 | 3 | 4;
  created_at?: string;
  has_reminder?: boolean;
}

export interface Task extends TaskPayload {
  id: string;
  title: string;
  completed: boolean;
  due_date: string;
  priority: 1 | 2 | 3 | 4;
  created_at: string;
  has_reminder: boolean;
}

export interface Column {
  id: string;
  date: string;
  title: string;
  day: string;
  label: string;
  modalIsOpen: boolean;
}

export interface Notification {
  id: string;
  is_read: boolean;
  task: Task;
}

export interface TodoState {
  status: "idle" | "loading" | "failed" | "succeeded";
  tasks: Task[];
  columns: Column[];
  notifications: Notification[];
  error: string | null;
  haveError: boolean;
}
