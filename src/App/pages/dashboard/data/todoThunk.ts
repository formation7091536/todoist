import { createAsyncThunk } from "@reduxjs/toolkit";
import { toast } from "react-toastify";
import supabase from "../../../shared/supabase";
import { Notification, TaskPayload } from "./todo.types";

export const getAllTasks = createAsyncThunk(
  "todoProvider/getAllTasks",
  async () => {
    try {
      const { data, error } = await supabase.from("task").select();

      if (error) throw error;
      else return data;
    } catch (error) {
      toast.error("Something went wrong!", {
        position: "bottom-center",
        className: "toast",
      });
      return Promise.reject((error as { message: string }).message);
    }
  },
);

export const createTask = createAsyncThunk(
  "todoProvider/createTask",
  async (task: TaskPayload) => {
    try {
      const { data, error } = await supabase
        .from("task")
        .insert([{ ...task }])
        .select();

      if (error) throw error;
      else return data[0];
    } catch (error) {
      toast.error("Something went wrong!", {
        position: "bottom-center",
        className: "toast",
      });
      return Promise.reject((error as { message: string }).message);
    }
  },
);

export const markTaskAsDone = createAsyncThunk(
  "todoProvider/markTaskAsDone",
  async (id: string) => {
    try {
      const { data, error } = await supabase
        .from("task")
        .update({ completed: true })
        .eq("id", id)
        .select();

      if (error) throw error;
      toast.success("Task marked as done!", {
        position: "bottom-center",
        className: "toast",
      });
      return data[0];
    } catch (error) {
      toast.error("Something went wrong!", {
        position: "bottom-center",
        className: "toast",
      });
      return Promise.reject((error as { message: string }).message);
    }
  },
);

export const deleteTask = createAsyncThunk(
  "todoProvider/deleteTask",
  async (id: string) => {
    try {
      const { error } = await supabase.from("task").delete().eq("id", id);

      if (error) throw error;
      toast.success("Task deleted successfully!", {
        position: "bottom-center",
        className: "toast",
      });
      return { id };
    } catch (error) {
      toast.error("Something went wrong!", {
        position: "bottom-center",
        className: "toast",
      });
      return Promise.reject((error as { message: string }).message);
    }
  },
);

export const updateTask = createAsyncThunk(
  "todoProvider/updateTask",
  async (task: TaskPayload) => {
    try {
      const { data, error } = await supabase
        .from("task")
        .update({ ...task })
        .eq("id", task.id)
        .select();

      if (error) throw error;

      toast.success("Task updated successfully!", {
        position: "bottom-center",
        className: "toast",
      });
      return data[0];
    } catch (error) {
      toast.error("Something went wrong!", {
        position: "bottom-center",
        className: "toast",
      });
      return Promise.reject((error as { message: string }).message);
    }
  },
);

export const getNotifications = createAsyncThunk(
  "todoProvider/getNotifications",
  async () => {
    try {
      const { data, error } = await supabase
        .from("notifications")
        .select(
          `id, is_read, task(id, title, due_date, priority, completed, created_at)`,
        );

      if (error) throw error;
      return (data as unknown as Notification[]).filter(
        (notif: Notification) => !notif.task.completed,
      );
    } catch (error) {
      return Promise.reject((error as { message: string }).message);
    }
  },
);

export const markNotificationAsRead = createAsyncThunk(
  "todoProvider/markNotificationAsRead",
  async (id: string) => {
    try {
      const { error } = await supabase
        .from("notifications")
        .update({ is_read: true })
        .eq("id", id)
        .select();

      if (error) throw error;
      return { id };
    } catch (error) {
      return Promise.reject((error as { message: string }).message);
    }
  },
);
